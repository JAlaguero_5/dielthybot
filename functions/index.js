const functions = require('firebase-functions');
const express = require('express');
const cors = require('cors');
const Firestore = require('@google-cloud/firestore');
const PROJECT_ID = 'tfg-dielthy';
const COLLECTION_NAME = 'aditivos';
const firestore = new Firestore({
  projectId: PROJECT_ID,
  timestampsInSnapshots: true
});


const app = express();

app.use(cors({ origin: true }));

app.post('/', async (req, res) => {
  const isTelegramMessage = req.body
                          && req.body.message
                          && req.body.message.chat
                          && req.body.message.chat.id
                          && req.body.message.from
                          && req.body.message.from.first_name;

  if (isTelegramMessage) {

    const chat_id = req.body.message.chat.id;

    const received = req.body.message.text;

    if (received.includes('/e')) {
      const code = received.slice(2).trim();
      if (code && code.length) {
        const aditiveInfo = await firestore.collection(COLLECTION_NAME).doc(code).get();
        if (aditiveInfo.exists) {
          const data = aditiveInfo.data();
          const check = String.fromCodePoint(0x2705);
          const cross = String.fromCodePoint(0x274C);
          const herb = String.fromCodePoint(0x1F33F);
          const pig = String.fromCodePoint(0x1F416);
          const question = String.fromCodePoint(0x2753);
          const tube = String.fromCodePoint(0x1F52C);
          return res.status(200).send({
            method: 'sendMessage',
            chat_id,
            text: 'Código: E-' + data.numero + '\n' +
            'Clasificación: ' + data.clasificacion + '\n' +
            'Descripción: ' + data.descripcion + '\n' + 
            'Origen: ' +
              `${data.origen === 'Vegetal' ? herb
                  : data.origen === 'Animal' ? pig
                    : data.origen === 'Indeterminado' ? question
                      : data.origen === 'Químico o sintético' ? tube
                        : ''}` + 
              '\n' + 
            'Apto para celíacos: ' +
              `${String.fromCodePoint(0x1F35E)}${data.cel ? check : cross}` + '\n' +
            'Apto para veganos/vegetarianos: ' +
              `${String.fromCodePoint(0x1F96C)}${data.veg ? check : cross}` + '\n' +
            'Apto para intolerantes a la lactosa: ' +
              `${String.fromCodePoint(0x1F9C0)}${data.lac ? check : cross}`
          });
        } else {
          return res.status(200).send({
            method: 'sendMessage',
            chat_id,
            text: 'No existe un aditivo con ese código'
          });
        }
      } else {
        return res.status(200).send({
          method: 'sendMessage',
          chat_id,
          text: 'No se ha especificado un código de aditivo'
        });
      }
    } else if (received.includes('/start')) {
      return res.status(200).send({
        method: 'sendMessage',
        chat_id,
        text: 'Bienvenido al bot de Dielthy. Aquí podrás consultar la información de los aditivos. Para ' +
        'comenzar a usar el bot teclea el comando /e seguido de un espacio y el código del aditivo ' +
        'que deseas consultar'
      });
    } else if (received.includes('/help')) {
      return res.status(200).send({
        method: 'sendMessage',
        chat_id,
        text: 'Para comenzar a usar el bot teclea el comando /e seguido de un espacio y el código del aditivo ' +
        'que deseas consultar'
      });
    } else {
      return res.status(200).send({
        method: 'sendMessage',
        chat_id,
        text: 'Para consultar cómo utilizar el bot envía /help'
      });
    }
  }

  return res.status(200).send({ status: 'No es un mensaje de Telegram' })
});

exports.router = functions.https.onRequest(app);
